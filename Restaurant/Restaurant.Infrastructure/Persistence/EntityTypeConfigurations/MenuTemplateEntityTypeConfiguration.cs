﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class MenuTemplateEntityTypeConfiguration : IEntityTypeConfiguration<MenuTemplate>
    {
        public void Configure(EntityTypeBuilder<MenuTemplate> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Restaurant)
                .WithMany(p => p.MenuTemplates)
                .HasForeignKey(p => p.RestaurantId);
        }
    }
}
