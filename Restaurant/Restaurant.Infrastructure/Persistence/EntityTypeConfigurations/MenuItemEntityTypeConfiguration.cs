﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class MenuItemEntityTypeConfiguration : IEntityTypeConfiguration<MenuItem>
    {
        public void Configure(EntityTypeBuilder<MenuItem> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Type)
                .WithMany(p => p.MenuItems)
                .HasForeignKey(p => p.TypeId);

            builder.HasOne(p => p.Restaurant)
                .WithMany(p => p.MenuItems)
                .HasForeignKey(p => p.RestaurantId);
        }
    }
}
