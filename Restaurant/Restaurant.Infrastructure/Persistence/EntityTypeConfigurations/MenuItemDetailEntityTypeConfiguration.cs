﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class MenuItemDetailEntityTypeConfiguration : IEntityTypeConfiguration<MenuItemDetail>
    {
        public void Configure(EntityTypeBuilder<MenuItemDetail> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.MenuTemplate)
                .WithMany(p => p.MenuItemDetails)
                .HasForeignKey(p => p.MenuTemplateId);

            builder.HasOne(p => p.MenuItem)
                .WithMany(p => p.MenuItemDetails)
                .HasForeignKey(p => p.MenuItemId);
        }
    }
}
