﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class OrderedServiceEntityTypeConfiguration : IEntityTypeConfiguration<OrderedService>
    {
        public void Configure(EntityTypeBuilder<OrderedService> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Order)
                .WithMany(p => p.OrderedServices)
                .HasForeignKey(p => p.OrderId);

            builder.HasOne(p => p.Service)
                .WithMany(p => p.OrderedServices)
                .HasForeignKey(p => p.ServiceId);
        }
    }
}
