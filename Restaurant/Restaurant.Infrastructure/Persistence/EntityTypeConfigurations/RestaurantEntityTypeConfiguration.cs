﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class RestaurantEntityTypeConfiguration : IEntityTypeConfiguration<Restaurants>
    {
        public void Configure(EntityTypeBuilder<Restaurants> builder)
        {
            builder.HasKey(p => p.Id);
        }
    }
}
