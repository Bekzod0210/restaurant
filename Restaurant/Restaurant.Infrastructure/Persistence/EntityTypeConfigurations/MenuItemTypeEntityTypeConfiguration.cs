﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class MenuItemTypeEntityTypeConfiguration : IEntityTypeConfiguration<MenuItemType>
    {
        public void Configure(EntityTypeBuilder<MenuItemType> builder)
        {
            builder.HasKey(p => p.Id);
        }
    }
}
