﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence.EntityTypeConfigurations
{
    public class OrderedMenuEntityTypeConfiguration : IEntityTypeConfiguration<OrderedMenu>
    {
        public void Configure(EntityTypeBuilder<OrderedMenu> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Order)
                .WithMany(p => p.OrderedMenus)
                .HasForeignKey(p => p.OrderId);

            builder.HasOne(p => p.MenuItem)
                .WithMany(p => p.OrderedMenus)
                .HasForeignKey(p => p.MenuItemId);
        }
    }
}
