﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Restaurant.Application.Abstractions;
using Restaurant.Domain;
using Restaurant.Domain.Entities;

namespace Restaurant.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) { }

        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<MenuItemDetail> MenuItemDetails { get; set; }
        public DbSet<MenuItemType> MenuItemTypes { get; set; }
        public DbSet<MenuTemplate> MenuTemplates { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderedMenu> OrderedMenus { get; set; }
        public DbSet<OrderedService> OrderedServices { get; set; }
        public DbSet<Restaurants> Restaurants { get; set; }
        public DbSet<RestaurantPrice> RestaurantPrices { get; set; }
        public DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }
    }
}
