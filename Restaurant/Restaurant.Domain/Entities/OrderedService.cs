﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Domain.Entities
{
    public class OrderedService
    {
        public int Id { get; set; }

        public int ServiceId { get; set; }
        public int OrderId { get; set; }

        public Service Service { get; set; }
        public Order Order { get; set; }
    }
}
