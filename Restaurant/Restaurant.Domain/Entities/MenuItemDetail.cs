﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Domain.Entities
{
    public class MenuItemDetail
    {
        public int Id { get; set; }

        public int MenuTemplateId { get; set; }
        public int MenuItemId { get; set; }

        public MenuTemplate MenuTemplate { get; set; }
        public MenuItem MenuItem { get; set; }
    }
}
