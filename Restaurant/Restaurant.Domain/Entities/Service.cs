﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restaurant.Domain.Enums;

namespace Restaurant.Domain.Entities
{
    public class Service
    {
        public Service()
        {
            OrderedServices = new HashSet<OrderedService>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public EventType EventType { get; set; }

        public int RestaurantId { get; set; }

        public Restaurants Restaurant { get; set; }
        public ICollection<OrderedService> OrderedServices { get; set; }
    }
}
