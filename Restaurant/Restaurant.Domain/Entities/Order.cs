﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Domain.Entities
{
    public class Order
    {
        public Order()
        {
            OrderedMenus = new HashSet<OrderedMenu>();
            OrderedServices = new HashSet<OrderedService>();
        }

        public int Id { get; set; }
        public int Quantity { get; set; }
        public double TotalPrice { get; set; }
        public DateTime CreatedDate { get; set; }

        public int ClientId { get; set; }   
        public int RestaurntId { get; set; }

        public Restaurants Restaurant { get; set; }
        public ICollection<OrderedMenu> OrderedMenus { get; set; }
        public ICollection<OrderedService> OrderedServices { get; set; }
    }
}
