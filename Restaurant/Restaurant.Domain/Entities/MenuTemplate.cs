﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Domain.Entities
{
    public class MenuTemplate
    {
        public MenuTemplate()
        {
            MenuItemDetails = new HashSet<MenuItemDetail>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string VideUrl { get; set; }

        public int RestaurantId { get; set; }
        
        public Restaurants Restaurant { get; set; }
        public ICollection<MenuItemDetail> MenuItemDetails { get; set; }
    }
}
