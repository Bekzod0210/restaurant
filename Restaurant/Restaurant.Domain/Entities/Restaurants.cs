﻿using Restaurant.Domain.Entities;

namespace Restaurant.Domain
{
    public class Restaurants
    {
        public Restaurants()
        {
            MenuItems = new HashSet<MenuItem>();
            MenuTemplates = new HashSet<MenuTemplate>();
            Orders = new HashSet<Order>();
            RestaurantPrices = new HashSet<RestaurantPrice>();
            Services = new HashSet<Service>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ChairNumber { get; set; }
        public string location { get; set; }
        public string ImageUrl { get; set; }

        public int SupplierId { get; set; }

        public ICollection<MenuTemplate> MenuTemplates { get; set; }
        public ICollection<MenuItem> MenuItems { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<RestaurantPrice> RestaurantPrices { get; set;}
        public ICollection<Service> Services { get; set; }
    }
}