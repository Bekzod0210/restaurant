﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.Domain.Entities
{
    public class MenuItem
    {
        public MenuItem()
        {
            MenuItemDetails = new HashSet<MenuItemDetail>();
            OrderedMenus = new HashSet<OrderedMenu>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }     
        public double Price { get; set; }
        public string ImageUrl { get; set; }

        public int TypeId { get; set; }
        public int RestaurantId { get; set; }

        public MenuItemType Type { get; set; }
        public Restaurants Restaurant { get; set; }
        public ICollection<MenuItemDetail> MenuItemDetails { get; set; }
        public ICollection<OrderedMenu> OrderedMenus { get; set; }
    }
}
