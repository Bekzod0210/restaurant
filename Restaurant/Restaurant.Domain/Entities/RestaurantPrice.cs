﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restaurant.Domain.Enums;

namespace Restaurant.Domain.Entities
{
    public class RestaurantPrice
    {
        public int Id { get; set; }
        public double ChairPrice { get; set; }
        public EventType EventType { get; set; }

        public int RestaurantId { get; set; }

        public Restaurants Restaurant { get; set; }
    }
}
