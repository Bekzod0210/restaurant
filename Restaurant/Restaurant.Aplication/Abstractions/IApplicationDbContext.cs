﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Restaurant.Domain;
using Restaurant.Domain.Entities;

namespace Restaurant.Application.Abstractions
{
    public interface IApplicationDbContext
    {
        DbSet<MenuItem> MenuItems { get; set; }
        DbSet<MenuItemDetail> MenuItemDetails { get; set; }
        DbSet<MenuItemType> MenuItemTypes { get; set; } 
        DbSet<MenuTemplate> MenuTemplates { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<OrderedMenu> OrderedMenus { get; set; }
        DbSet<OrderedService> OrderedServices { get; set; }
        DbSet<Restaurants> Restaurants { get; set; }
        DbSet<RestaurantPrice> RestaurantPrices { get; set; }
        DbSet<Service> Services { get; set; }


        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
